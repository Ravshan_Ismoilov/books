from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model


class CustomUserCreationForm(UserCreationForm):
    """CustomUserCreationForm definition."""
    class Meta:
        model = get_user_model()
        fields = ('email', 'username',)

class CustomUserChangeForm(UserChangeForm):
    """CustomUserChangeForm definition."""
    class Meta:
        model = get_user_model()
        fields = ('email', 'username',)